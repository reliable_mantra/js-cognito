## sample-webpack-app
A sample production-ready app using webpack and vanilla JS.  

To start the development environment, run `npm install` and `npm run dev` 

To deploy the production app to heroku, run `heroku create [app-name]`, then `git push heroku master`, then `heroku open` 

## Cognito integration
Just change the config at src/index.ts line 14:

```
Auth.configure({
  userPoolId: 'user-pool-id',
  userPoolWebClientId: 'client-id',
  oauth: {
    region: 'eu-central-1',
    domain: 'user-pool-subdomain.auth.eu-central-1.amazoncognito.com',
    scope: ['email', 'openid', 'aws.cognito.signin.user.admin'],
    redirectSignIn: 'https://127.0.0.1:8080',
    redirectSignOut: 'https://127.0.0.1:8080',
    responseType: 'code' // or 'token', note that REFRESH token will only be generated when the responseType is code
  }
})
```
